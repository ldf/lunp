/* 
 * getaddrinfo.c - Simple example of using getaddrinfo(3) function.
 * 
 * Michal Ludvig <michal@logix.cz> (c) 2002, 2003
 * http://www.logix.cz/michal/devel/
 *
 * License: public domain.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "../../lib/lunp.h"

int
main (int argc, char *argv[])
{
  if (argc < 2)
    exit (1);
  char* res = lookup_host (argv[1]);
  if(res != NULL){
    printf("Ret address: %s\n", res);
  }
  else{
    printf("No IP address has been found");
  }
  return 0;
}
