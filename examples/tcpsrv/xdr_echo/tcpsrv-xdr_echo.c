#include "../../../lib/lunp.h"
#include <rpc/types.h>
#include <rpc/xdr.h>

#define BS 1024
#define IBS 1024
#define OBS 1024

#define MAXSTR 1024

void
sig_int(int sig){
  exit(0);
}

void
child_task(int sockfd)
{
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             n;
  count = 0;

  /* XDR */
  XDR xdrs_in;
  XDR xdrs_out;
  FILE *xfp_r;
  FILE *xfp_w;
  char *xdr_obuf1;
  char *xdr_obuf2;
  char *xdr_ibuf;

  /* XDR Bootstrap*/
  xfp_r = Fdopen(sockfd, "r");
  xdrstdio_create(&xdrs_in, xfp_r, XDR_DECODE);

  xfp_w = Fdopen(sockfd, "w");
  xdrstdio_create(&xdrs_out, xfp_w, XDR_ENCODE);

  setbuf(xfp_w, NULL);
  setbuf(xfp_r, NULL);

    /* Receiving */
  xdr_ibuf = NULL;
  xdr_string(&xdrs_in, &xdr_ibuf, IBS);
  printf("Received: %s\n", xdr_ibuf);

    /* Sending */
  xdr_obuf1 = strdup(xdr_ibuf);
  if(!xdr_string(&xdrs_out, &xdr_obuf1, OBS)){
    printf("Error\n");
  }

  count++;
  fprintf(stdout, "%d datagram(s) sent\n", count);

  xdr_ibuf = NULL;

  xdr_destroy(&xdrs_in);
  xdr_destroy(&xdrs_out);
  return;
}


int
main(int argc, char **argv)
{
  int         listenfd, connfd;
  pid_t       childpid;
  socklen_t     clilen;
  struct sockaddr_in  cliaddr, servaddr;
  void        sig_chld(int);

  listenfd = Socket(AF_INET, SOCK_STREAM, 0);

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(atoi(argv[1]));
  }else
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);

  Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

  Listen(listenfd, LISTENQ);

  Signal(SIGINT, sig_int);

  for ( ; ; ) {
    fprintf(stdout, "Tcp server starts.\n");
    clilen = sizeof(cliaddr);
    if ( (connfd = accept(listenfd, (SA *) &cliaddr, &clilen)) < 0) {
      if (errno == EINTR)
        continue;   /* back to for() */
      else
        err_sys("accept error");
    }
    child_task(connfd); /* process the request */
    fprintf(stdout, "Tcp server ends.\n\n");
    Close(connfd);      /* parent closes connected socket */
  }

  exit(0);
}