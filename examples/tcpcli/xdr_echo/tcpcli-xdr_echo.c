#include "../../../lib/lunp.h"
#include <rpc/types.h>
#include <rpc/xdr.h>

#define BS 1024
#define IBS 1024
#define OBS 1024

#define MAXSTR 1024

void
dg_cli(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            tbuf[BS+1];
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             i, idx, n;
  count = 0;
  len = clilen;

  /* XDR */
  XDR xdrs_in;
  XDR xdrs_out;
  FILE *xfp_r;
  FILE *xfp_w;
  char *xdr_obuf1;
  char *xdr_obuf2;
  char *xdr_ibuf;

  /* XDR Bootstrap*/
  xfp_r = Fdopen(sockfd, "r");
  xdrstdio_create(&xdrs_in, xfp_r, XDR_DECODE); 

  xfp_w = Fdopen(sockfd, "w");
  xdrstdio_create(&xdrs_out, xfp_w, XDR_ENCODE); 

  setbuf(xfp_w, NULL);
  setbuf(xfp_r, NULL);
  
  fprintf(stdout, "Udp client starts\n");

  /* Getting the input from stdin */
  fprintf(stdout, "Enter string (max %d char):\n", MAXSTR);
  fprintf(stdout, "> ");
  fgets(tbuf, IBS, stdin);
  idx = strlen(tbuf);
  for(i = idx; i < MAXSTR; i++) tbuf[i]='\0';
  if(idx > MAXSTR){
    fprintf(stdout, "The input string is greater then the maximun (%d).\n", MAXSTR);
    return;
  }

  fprintf(stdout, "Input string is correct.\n");
  /* Sending */
  xdr_obuf1 = strdup(tbuf);
  if(!xdr_string(&xdrs_out, &xdr_obuf1, OBS)){
    printf("Error\n");
  }

  /* Receiving */
  xdr_ibuf = NULL;
  if(!xdr_string(&xdrs_in, &xdr_ibuf, IBS)){
    printf("Error\n");
  }
  fprintf(stdout, "Server has replied: %s\n", xdr_ibuf);

  count++;
  fprintf(stdout, "%d datagram(s) sent\n", count);

  xdr_destroy(&xdrs_in);
  xdr_destroy(&xdrs_out);
  fprintf(stdout, "Udp client ends\n");
  return;
}

int main(int argc, char * argv[]){
  
  int                     sockfd;
  struct sockaddr_in      servaddr;

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(atoi(argv[1]));
    Inet_pton(AF_INET, LOOPBACK_STR, &servaddr.sin_addr);
  }else if(argc == 3){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(atoi(argv[2]));
    Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
  }else
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);

  sockfd = Socket(AF_INET, SOCK_STREAM, 0);
  Connect(sockfd, (SA *) &servaddr, sizeof(servaddr));

  dg_cli(stdin, sockfd, (SA *) &servaddr, sizeof(servaddr));
  close(sockfd);

  return 0;
}
