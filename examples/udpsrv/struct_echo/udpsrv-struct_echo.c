#include "../../../lib/lunp.h"

#define BS 1024
#define IBS 1024
#define OBS 1024

#define MAXSTR 1024


/* START INTEGERS*/
typedef struct integers
{
  int id;
  int op1;
  int op2;
} integers_t;

integers_t integers_obj;
integers_t response_integers_obj;

void
dg_srv_integers(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            tbuf[BS+1];
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             n;
  count = 0;
  
  fprintf(stdout, "Udp server starts\n");
  for ( ; ; ) {
    len = clilen;

    /* Receiving */
    n = Recvfrom(sockfd, &integers_obj, sizeof(integers_obj), 0, cliaddr, &len);
    fprintf(stdout, "Received %d bytes\n", n);
    

    /* Computing */
    // Gathering NtoH
    response_integers_obj.id  = ntohl(integers_obj.id);
    response_integers_obj.op1 = ntohl(integers_obj.op1);
    response_integers_obj.op2 = ntohl(integers_obj.op2);

    // [...]
    integers_toString(response_mixed_obj);

    // Preparing response HtoN
    response_integers_obj.id  = htonl(response_integers_obj.id);
    response_integers_obj.op1 = htonl(response_integers_obj.op1);
    response_integers_obj.op2 = htonl(response_integers_obj.op2);

    /* Sending */
    Sendto(sockfd, &response_integers_obj, sizeof(response_integers_obj), 0, cliaddr, len);

    count++;
    fprintf(stdout, "%d datagram(s) sent\n", count);
  }
  fprintf(stdout, "Udp server ends\n");
  return;
}


/* START MIXED*/
#define MSG 5

typedef struct mixed
{
  int id;
  char msg[MSG];
  int num;
} mixed_t;

mixed_t mixed_obj;
mixed_t response_mixed_obj;

void mixed_toString(mixed_t obj){
  printf("id = %d msg = %s num = %d\n", obj.id, obj.msg, obj.num);
}

void
dg_srv_mixed(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            tbuf[BS+1];
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             i, n;
  count = 0;
  
  fprintf(stdout, "Udp server starts\n");
  for ( ; ; ) {
    len = clilen;

    /* Receiving */
    n = Recvfrom(sockfd, &mixed_obj, sizeof(mixed_obj), 0, cliaddr, &len);
    fprintf(stdout, "Received %d bytes\n", n);
    

    /* Computing */
    // Gathering NtoH
    response_mixed_obj.id  = ntohl(mixed_obj.id);
    for(i = 0; i < MSG && mixed_obj.msg[i]!='\0'; i++){
      response_mixed_obj.msg[i] = mixed_obj.msg[i];
    }
    response_mixed_obj.num = ntohl(mixed_obj.num);

    // [...]
    mixed_toString(response_mixed_obj);

    // Preparing response HtoN
    response_mixed_obj.id  = htonl(response_mixed_obj.id);
    response_mixed_obj.num = htonl(response_mixed_obj.num);

    /* Sending */
    Sendto(sockfd, &response_mixed_obj, sizeof(response_mixed_obj), 0, cliaddr, len);

    count++;
    fprintf(stdout, "%d datagram(s) sent\n", count);
  }
  fprintf(stdout, "Udp server ends\n");
  return;
}


int
main(int argc, char **argv)
{
  int                     sockfd;
  struct sockaddr_in      servaddr, cliaddr;

  if(argc == 4){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(argv[1]);
    servaddr.sin_port        = htons(atoi(argv[2]));
  }else{
    err_quit("usage: %s <IPaddress> <port#> <-i|-m>", argv[0]);
  }

  sockfd = Socket(AF_INET, SOCK_DGRAM, 0);
  Bind(sockfd, (SA *) &servaddr, sizeof(servaddr));

  if(strcmp(argv[3], "-i")==0){
    dg_srv_integers(stdin, sockfd, (SA *) &cliaddr, sizeof(cliaddr));
  }else if(strcmp(argv[3], "-m")==0){
    dg_srv_mixed(stdin, sockfd, (SA *) &cliaddr, sizeof(cliaddr));
  }else{
    err_quit("usage: %s <IPaddress> <port#> <-i|-m>", argv[0]);
  }
  exit(0);
}


