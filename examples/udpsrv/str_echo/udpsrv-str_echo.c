#include "../../../lib/lunp.h"

#define BS 1024
#define IBS 1024
#define OBS 1024

#define MAXSTR 1024

void
dg_srv(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             n;
  count = 0;
  
  fprintf(stdout, "Udp server starts\n");
  for ( ; ; ) {
    len = clilen;

    /* Receiving */
    n = Recvfrom(sockfd, ibuf, IBS, 0, cliaddr, &len);
    fprintf(stdout, "Received %d bytes\n", n);
    
    ibuf[strlen(ibuf)] = '\0';

    /* Sending */
    strncpy(obuf, ibuf, n);
    Sendto(sockfd, obuf, n, 0, cliaddr, len);

    count++;
    fprintf(stdout, "%d datagram(s) sent\n", count);
  }
  fprintf(stdout, "Udp server ends\n");
  return;
}


int
main(int argc, char **argv)
{
  int                     sockfd;
  struct sockaddr_in      servaddr, cliaddr;

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(atoi(argv[1]));
  }else if(argc == 3){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(argv[1]);
    servaddr.sin_port        = htons(atoi(argv[2]));
  }else
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);

  sockfd = Socket(AF_INET, SOCK_DGRAM, 0);
  Bind(sockfd, (SA *) &servaddr, sizeof(servaddr));

  dg_srv(stdin, sockfd, (SA *) &cliaddr, sizeof(cliaddr));

  exit(0);
}


