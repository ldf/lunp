#include "../../../lib/lunp.h"

#define BS 1024
#define IBS 1024
#define OBS 1024

#define MAXSTR 1024

/* START INTEGERS */
typedef struct integers{
  int id;
  int op1;
  int op2;
} integers_t;

integers_t integers_obj;
integers_t response_integers_obj;

void integers_toString(integers_t obj){
  printf("id = %d op1 = %d op2 = %d\n", obj.id, obj.op1, obj.op2);
}
void integers_toString_ntoh(integers_t obj){
  printf("id = %d op1 = %d op2 = %d\n", ntohl(obj.id), ntohl(obj.op1), ntohl(obj.op2));
}
void integers_toString_hton(integers_t obj){
  printf("id = %d op1 = %d op2 = %d\n", htonl(obj.id), htonl(obj.op1), htonl(obj.op2));
}

void
dg_cli_integers(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            tbuf[BS+1];
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             i, idx, n;
  count = 0;
  len = clilen;

  fprintf(stdout, "Udp server starts\n");

  /* Getting the input from stdin */
  fprintf(stdout, "Enter <id> <op1> <op2>:\n", MAXSTR);
  fprintf(stdout, "> ");
  fgets(tbuf, IBS, stdin);
  idx = strlen(tbuf);
  for(i = idx; i < MAXSTR; i++) tbuf[i]='\0';
  if(idx > MAXSTR){ // ERROR
    fprintf(stdout, "The input string is greater then the maximun (%d).\n", MAXSTR);
    return;
  }
  
  /* Data preparation */
  char str1[BS+1];
  char str2[BS+1];
  char str3[BS+1];
  sscanf(tbuf, "%s %s %s", str1, str2, str3);
  integers_obj.id  = htonl(atoi(str1));
  integers_obj.op1 = htonl(atoi(str2));
  integers_obj.op2 = htonl(atoi(str3));
    
  /* Sending */
  Sendto(sockfd, &integers_obj, sizeof(integers_obj), 0, cliaddr, len);

  /* Receiving */
  n = Recvfrom(sockfd, &response_integers_obj, sizeof(response_integers_obj), 0, cliaddr, &len);
  fprintf(stdout, "Received %d bytes\n", n);

  /* Response preparation*/
  response_integers_obj.id  = ntohl(response_integers_obj.id);
  response_integers_obj.op1 = ntohl(response_integers_obj.op1);
  response_integers_obj.op2 = ntohl(response_integers_obj.op2);


  fprintf(stdout, "Server has replied: ");
  integers_toString(response_integers_obj);

  count++;
  fprintf(stdout, "%d datagram(s) sent\n", count);

  fprintf(stdout, "Udp server ends\n");
  return;
}

/* START MIXED */
#define MSG 5

typedef struct mixed{
  int id;
  char msg[MSG];
  int num;
} mixed_t;

mixed_t mixed_obj;
mixed_t response_mixed_obj;

void mixed_toString(mixed_t obj){
  printf("id = %d msg = %s num = %d\n", obj.id, obj.msg, obj.num);
}

void
dg_cli_mixed(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            tbuf[BS+1];
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             i, idx, n;
  count = 0;
  len = clilen;

  fprintf(stdout, "Udp server starts\n");

  /* Getting the input from stdin */
  fprintf(stdout, "Enter <id> <msg>[max %d char] <num>:\n", MAXSTR, MSG);
  fprintf(stdout, "> ");
  fgets(tbuf, IBS, stdin);
  idx = strlen(tbuf);
  for(i = idx; i < MAXSTR; i++) tbuf[i]='\0';
  if(idx > MAXSTR){
    fprintf(stdout, "The input string is greater then the maximun (%d).\n", MAXSTR);
    return;
  }
        
  /* Data preparation */
  char str1[BS+1];
  char str2[BS+1];
  char str3[BS+1];
  sscanf(tbuf, "%s %s %s", str1, str2, str3);
  mixed_obj.id  = htonl(atoi(str1));
  for(i = 0; i < MSG && str2[i]!='\0'; i++){
    mixed_obj.msg[i] = str2[i];
  }
  mixed_obj.num = htonl(atoi(str3));

  /* Sending */
  Sendto(sockfd, &mixed_obj, sizeof(mixed_obj), 0, cliaddr, len);

  /* Receiving */
  n = Recvfrom(sockfd, &response_mixed_obj, sizeof(response_mixed_obj), 0, cliaddr, &len);
  fprintf(stdout, "Received %d bytes\n", n);

  /* Response preparation*/
  response_mixed_obj.id = ntohl(response_mixed_obj.id);
  response_mixed_obj.num = ntohl(response_mixed_obj.num);

  fprintf(stdout, "Server has replied: ");
  mixed_toString(response_mixed_obj);

  count++;
  fprintf(stdout, "%d datagram(s) sent\n", count);

  fprintf(stdout, "Udp server ends\n");
  return;
}


int main(int argc, char * argv[]){
  
  int                     sockfd;
  struct sockaddr_in      servaddr;

  if(argc == 4){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(atoi(argv[2]));
    Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
  }else
    err_quit("usage: %s <IPaddress> <port#> <-i|-m>", argv[0]);

  sockfd = Socket(AF_INET, SOCK_DGRAM, 0);

  if(strcmp(argv[3], "-i")==0){
    dg_cli_integers(stdin, sockfd, (SA *) &servaddr, sizeof(servaddr));
  }else if(strcmp(argv[3], "-m")==0){
    dg_cli_mixed(stdin, sockfd, (SA *) &servaddr, sizeof(servaddr));
  }else
    err_quit("usage: %s [<IPaddress>] <port#> <-i|-m>", argv[0]);


  return 0;
}
