#include	"lunp.h"

/* 
   In case of success, it returns the FIRST valid IP address in dotted decimal notation as char * format bounded to the specified domain name 
   In case no IP address is found, it returns NULL.
*/
char *
lookup_host (const char *host)
{
  struct addrinfo hints, *res;
  char *first_ipv4_address = NULL;
  int errcode;
  char addrstr[100];
  void *ptr;
  int flag = 0;

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;

  errcode = getaddrinfo (host, NULL, &hints, &res);
  if (errcode != 0)
    {
      perror ("getaddrinfo");
      return NULL;
    }

  printf ("Host: %s\n", host);
  while (res)
    {
      inet_ntop (res->ai_family, res->ai_addr->sa_data, addrstr, 100);

      switch (res->ai_family)
        {
        case AF_INET:
          ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
	  inet_ntop (res->ai_family, ptr, addrstr, 100);
	  if(flag == 0){
	    first_ipv4_address = strdup(addrstr); 
	    flag = 1;
	  }
          break;
        case AF_INET6:
          ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
	  inet_ntop (res->ai_family, ptr, addrstr, 100);
          break;
        }
      printf ("IPv%d address: %s (%s)\n", res->ai_family == PF_INET6 ? 6 : 4,
              addrstr, res->ai_canonname);
      res = res->ai_next;
    }

  return first_ipv4_address;
}


/* Return uint16_t value if correctly received, -1 if error occurred */
int check_uint16(int value, uint16_t *retval){
  int max_value = 65536-1;                // 2^16-1
  printf("Value: %d\n", value);
  if(value > max_value) return -1;
  if(value < 0) return -1;
  *retval = (uint16_t) value;
  return 0;
}

/* Return 0 if correctly received, -1 if error occurred */
int get_uint16(char * tbuf, uint16_t *uint16){
  int i;
  int value;
  int max_value = 65536-1;                // 2^16-1
  if(tbuf == NULL){
    printf("get_uint16 function: ERROR <the string is NULL>\n");
    return -1;
  }
  /* Checking if it is below 5 char */
  printf("get_uint16 function: string=%s, length=%d\n", tbuf, (int)strlen(tbuf));
  if(strlen(tbuf) == 0 || strlen(tbuf) > 5){
    printf("get_uint16 function: ERROR <too much char (max  5)>\n");
    return -1;
  }
  /* Checking if tbuf is a number */
  i = 0;
  while(tbuf[i]!='\0' && isdigit(tbuf[i])) i++;
  if(tbuf[0] == '-'){
    printf("get_uint16 function: ERROR <value is negative>\n");
    return -1;
  }
  if(i != strlen(tbuf)){
    printf("get_uint16 function: ERROR <not all char are isdigit compatible>\n");
    return -1;      //FAIL tbuf is not even a number
  }

  /* Checking parsing it as an integer */
  sscanf(tbuf, "%d", &value);
  if(value > max_value){
    printf("get_uint16 function: ERROR <value is greather than the maximum number (65536)>\n");
    return -1;
  }
  sscanf(tbuf, "%hu", uint16);
  
  return 0;
}
/* Return uint32_t value if correctly received, -1 if error occurred */
int check_uint32(int value, uint32_t *retval){
  int max_value = 4294967296-1;                // 2^32-1
  printf("Value: %d\n", value);
  if(value > max_value) return -1;
  if(value < 0) return -1;
  *retval = (uint32_t) value;
  return 0;
}
/* Return 0 if correctly received, -1 if error occurred */
int get_uint32(char * tbuf, uint32_t *uint32){
  int i;
  int value;
  int max_value = 4294967296-1;                // 2^32-1
    if(tbuf == NULL){
    printf("get_uint32 function: ERROR <the string is NULL>\n");
    return -1;
  }
  /* Checking if it is below 10 char */
  printf("get_uint32 function: string=%s, length=%d\n", tbuf, (int)strlen(tbuf));
  if(strlen(tbuf) == 0 || strlen(tbuf) > 10){
    printf("get_uint32 function: ERROR <too much char (max 10)>\n");
    return -1;
  }
    if(tbuf[0] == '-'){
    printf("get_uint32 function: ERROR <value is negative>\n");
    return -1;
  }
  /* Checking if tbuf is a number */
  i = 0;
  while(tbuf[i]!='\0' && isdigit(tbuf[i])) i++;
  if(i != strlen(tbuf)) return -1;      //FAIL tbuf is not even a number
  /* Checking parsing it as an integer */
  sscanf(tbuf, "%d", &value);
  if(value > max_value){
    printf("get_uint32 function: ERROR <value is greather than the maximum number (4294967295)>\n");
    return -1;
  }
  sscanf(tbuf, "%"PRIu32, uint32);
  
  return 0;
}

void str_append(char *src, char *app){
  int i;
  int len = strlen(src);
  char *ptr = &(src[len]);

  for(i = 0; i<strlen(app); i++) {
    *ptr = app[i];
    ptr ++;
  }
  *ptr = '\0';

  return;
}
