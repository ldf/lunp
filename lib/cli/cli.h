#ifndef __clih
#define __clih
#include "../lunp.h"

int
tcpcli_simple(int argc, char *argv[], void (*client_task)(int));

int
udpcli_simple(int argc, char **argv, void(*dg_cli)(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen));


#endif
