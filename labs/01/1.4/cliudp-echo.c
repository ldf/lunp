#include "../../../lib/lunp.h"

#define IBS 1024
#define OBS 1024
#define BS    80

#define MAXSTR    31

void
dg_cli(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            tbuf[BS+1];
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             i, idx, n;
  count = 0;
  
  fprintf(stdout, "Udp server starts\n");
  for ( ; ; ) {
    len = clilen;

    /* Getting the input from stdin */
    fprintf(stdout, "Enter string (max %d char):\n", MAXSTR);
    fprintf(stdout, "> ");
    fgets(tbuf, IBS, stdin);
    idx = strlen(tbuf);
    for(i = idx; i < MAXSTR; i++) tbuf[i]='\0';
    if(idx > MAXSTR){
      fprintf(stdout, "The input string is greater then the maximun (%d).\n", MAXSTR);
      fprintf(stdout, "Please retry.\n");
    }else{
      fprintf(stdout, "Input string is correct.\n");
      /* Sending */
      strncpy(obuf, tbuf, idx);
      Sendto(sockfd, obuf, idx, 0, cliaddr, len);

      /* Receiving */
      n = Recvfrom(sockfd, ibuf, MAXLINE, 0, cliaddr, &len);
      fprintf(stdout, "Received %d bytes\n", n); 
      strncpy(tbuf, ibuf, n);
      fprintf(stdout, "Server has replied: %s\n", tbuf);

      count++;
      fprintf(stdout, "%d datagram(s) sent\n", count);
    }
  }
  fprintf(stdout, "Udp server ends\n");
  return;
}


int main(int argc, char *argv[]){
  int                     sockfd;
  struct sockaddr_in      servaddr;

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(atoi(argv[1]));
    Inet_pton(AF_INET, LOOPBACK_STR, &servaddr.sin_addr);
  }else if(argc == 3){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(atoi(argv[2]));
    Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
  }else
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);

  sockfd = Socket(AF_INET, SOCK_DGRAM, 0);

  dg_cli(stdin, sockfd, (SA *) &servaddr, sizeof(servaddr));

  return 0;
}
