#include "../../../lib/lunp.h"

#define IBS 1024
#define OBS 1024

void
dg_srv(FILE *ifp, int sockfd, SA *cliaddr, socklen_t clilen)
{
  socklen_t       len;
  char            ibuf[IBS+1];
  char            obuf[OBS+1];
  int             count;
  int             n;
  count = 0;
  
  fprintf(stdout, "Udp server starts\n");
  for ( ; ; ) {
    len = clilen;

    /* Receiving */
    n = Recvfrom(sockfd, ibuf, MAXLINE, 0, cliaddr, &len);
    fprintf(stdout, "Received %d bytes\n", n);
    
    ibuf[strlen(ibuf)] = '\0';

    /* Computing */
    fprintf(stdout, "Client answered: %s\n", ibuf);

    /* Sending */
    strncpy(obuf, ibuf, n);
    Sendto(sockfd, obuf, n, 0, cliaddr, len);

    count++;
    fprintf(stdout, "%d datagram(s) sent\n", count);
  }
  fprintf(stdout, "Udp server ends\n");
  return;
}

#define SIGC 2 //Number of signal handled

void
sig_int(int signo){
  fprintf(stdout, "In the SIG_INT handler\n");
}

void
sig_usr1(int signo){
  fprintf(stdout, "In the SIG_USR1 handler\n");
}

int main(int argc, char *argv[]){
  int sigc = SIGC;
  void ( *sigv[2] ) (int) = {sig_int, sig_usr1};
  int intv[2] = {SIGINT, SIGUSR1};
  int                     sockfd;
  struct sockaddr_in      servaddr, cliaddr;
  int i;

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(atoi(argv[1]));
  }else if(argc == 3){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(argv[1]);
    servaddr.sin_port        = htons(atoi(argv[2]));
  }else
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);

  for (i = 0; i < sigc; i++)
    Signal(intv[i], sigv[i]);

  sockfd = Socket(AF_INET, SOCK_DGRAM, 0);
  Bind(sockfd, (SA *) &servaddr, sizeof(servaddr));

  dg_srv(stdin, sockfd, (SA *) &cliaddr, sizeof(cliaddr));

  return 0;
}
