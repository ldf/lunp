#include "../../../lib/lunp.h"
#define IBS 1024
#define OBS 1024
#define BS  1024
#define FALSE 0
#define TRUE  1

void client_task(int connfd){
  /* Socket variables */
  char obuf[OBS+1];
  char ibuf[IBS+1];
  ssize_t n;
  uint32_t size;

  /* Internal variables */
  char tbuf[BS+1];
  char ifn[BS+1];
  char command[BS+1];
  FILE *ifp;

  /* Flags*/
  int flag_quit = FALSE;

  /* Indexes */
  int i;

  /* Initialize the bread struct*/
  bread_t *bread;
  Bread_init(&bread, connfd);
  /* Path */
//  strcpy(ifn, "~/Documents/ldf-FTP/client/");
//  strcpy(ifn, "/export/home/stud/s171072/Documents/ldf_FTP/client/");
  strcpy(ifn, "~/Documents/ldf_FTP/client/");

  fprintf(stdout, "Client starts.\n");
  fprintf(stdout, "MENU'\n");
  fprintf(stdout, "GETfilename  -- to request a file\n");
  fprintf(stdout, "QUIT         -- to exit\n");

  /* Main loop MENU */
  while (!flag_quit) {
    fprintf(stdout, "> ");
    fscanf(stdin, "%s", obuf);
    strcat(obuf, "\n\0");

    if(strncmp(obuf, "GET", 3)==0){
      Writen(connfd, obuf, strlen(obuf));
      /* +OK\n\0 -->  5 characters */
      /* -ERR\n\0 --> 6 characters */
      Breadline(bread, ibuf, 6);
      /* Answer checking */
      if(strncmp(ibuf, "-ERR", 3)==0){
        fprintf(stdout, "File error!\n");
        Breadline(bread, ibuf, 1);
      }
      else if(strncmp(ibuf, "+OK", 3)==0){
        fprintf(stdout, "+OK receiving data!\n");
        /* Opening the file */
        strncat(ifn, obuf+3, strlen(obuf)-4);
        ifp = fopen(ifn, "wb");
        if (ifp==NULL) {
          fprintf(stderr, "File opening error!\n");
          return;
        }
        /* Read the size */
        Breadn(bread, ibuf, 4);
        memcpy(&size, ibuf, 4);
        size = ntohl(size);
        fprintf(stdout, "Size: %d\n", (int)size);
        /* file transfer */
        ssize_t received;
        ssize_t received_tot=0;
        long how_many;

        while (received_tot<size) {
          fprintf(stdout, "Byteleft %ld\n", size-received_tot);
          /* This prevent starvation */
          if ((size-received_tot)>IBS) {
            how_many = IBS-1;
          }else{
            how_many = size-received_tot;
          }
          received = Breadn(bread, ibuf, how_many);
          printf("received %zd\n", received);
          fwrite(ibuf, 1, received, ifp);
          received_tot+=received;
        }
        fclose(ifp);
        fprintf(stdout, "File transfer completed!\n");
      }
    }else if(strncmp(obuf, "QUIT", 4)==0){
      flag_quit = TRUE;
      Writen(connfd, obuf, strlen(obuf));
    }else{
     fprintf(stdout, "Non recognized command\n");
   }
 }
 fprintf(stdout, "Client ends.\n");
 return;
}


int main(int argc, char *argv[]){
  int     sockfd;
  struct sockaddr_in  servaddr;

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(atoi(argv[1]));
    Inet_pton(AF_INET, LOOPBACK_STR, &servaddr.sin_addr);
  }else if(argc == 3){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(atoi(argv[2]));
    Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
  }
  else{
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);
  }

  sockfd = Socket(AF_INET, SOCK_STREAM, 0);
  Connect(sockfd, (SA *) &servaddr, sizeof(servaddr));
  
  /* Perform the task - do all*/
  client_task(sockfd);

  Close(sockfd);
  return 0;
}