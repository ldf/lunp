#include "../../../lib/lunp.h"

#include <ctype.h>

#include <rpc/types.h>
#include <rpc/xdr.h>

#define IBS 1024
#define OBS 1024
#define BS    80

void
client_task(int sockfd){
  /* Bootstrap variables */
  /* Main variables */
  uint16_t   op1, op2;  
  uint16_t   res;
  char       tbuf[BS+1];
  char       op1buf[BS+1];
  char       op2buf[BS+1];
  int        quit;
  /* XDR */
  XDR xdrs_in;
  XDR xdrs_out;
  FILE *xfp_r;
  FILE *xfp_w;

  /* XDR Bootstrap*/
  xfp_r = Fdopen(dup(sockfd), "r");
  xdrstdio_create(&xdrs_in, xfp_r, XDR_DECODE); 

  xfp_w = Fdopen(dup(sockfd), "w");
  xdrstdio_create(&xdrs_out, xfp_w, XDR_ENCODE); 

  setbuf(xfp_w, NULL);
  setbuf(xfp_r, NULL);

  /* Main */
  fprintf(stdout, "Client starts.\n");

  /* Get op1 and op2 from stdin */
  quit = 0;
  while(!quit){
    op1 = 0; op1buf[0] = '\0';
    op2 = 0; op2buf[0] = '\0';
    fprintf(stdout, "Enter the two operands (unsigned int 16):\n");
    fprintf(stdout, "> ");
    fgets(tbuf, BS, stdin);
    sscanf(tbuf, "%s %s\n", op1buf, op2buf);
    /* Validating input */
    if(strncmp(op1buf, "QUIT", 4) == 0){
      fprintf(stdout, "Quitting the program.\n");
      quit = 1;
    }else if( get_uint16(op1buf, &op1)!=0 || get_uint16(op2buf, &op2)!=0 ){
      fprintf(stdout, "There is an error in one or both of the operands.\n");
    }else{
      fprintf(stdout, "Both operands are correct.\n");
      fprintf(stdout, "op1 = %hu\n", op1);
      fprintf(stdout, "op2 = %hu\n", op2);

      /* XDR transfer op1 and op2 */
      fprintf(stdout, "Sending message.\n");
      if(!xdr_uint16_t(&xdrs_out, &op1)){
	fprintf(stderr, "Error occurred while sending op1\n");
      }
      if(!xdr_uint16_t(&xdrs_out, &op2)){
	fprintf(stderr, "Error occurred while sending op2\n");
      }
      fprintf(stdout, "Message correctly sent.\n");

      /* XDR receive res */
      xdr_uint16_t(&xdrs_in, &res);
      fprintf(stdout, "Server answered: %d.\n", res);
    }
  }
  xdr_destroy(&xdrs_in);
  xdr_destroy(&xdrs_out);
  fclose(xfp_r);
  fclose(xfp_w);
  fprintf(stdout, "Client ends.\n");
  //exit(0);
}

int main(int argc, char* argv[]){
  int			sockfd;
  struct sockaddr_in	servaddr;

  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(atoi(argv[1]));
    Inet_pton(AF_INET, LOOPBACK_STR, &servaddr.sin_addr);
  }else if(argc == 3){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(atoi(argv[2]));
    Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
  }
  else{
    err_quit("usage: %s [<IPaddress>] <port#>", argv[0]);
  }

  sockfd = Socket(AF_INET, SOCK_STREAM, 0);
  Connect(sockfd, (SA *) &servaddr, sizeof(servaddr));
  
  /* Perform the task - do all*/
  client_task(sockfd);

  Close(sockfd);
  return 0;
}
