#include "../../../lib/lunp.h"
#include <ctype.h>

#include <rpc/types.h>
#include <rpc/xdr.h>

#define IBS 1024
#define OBS 1024
#define BS    80

int
server_task(int connfd){

  /* Main variables */
  uint16_t   op1, op2;  
  uint16_t   res;
  /* XDR */
  XDR        xdrs_in;
  XDR        xdrs_out;
  FILE       *xfp_r;
  FILE       *xfp_w;


  /* XDR Bootstrap*/
  xfp_r = Fdopen(dup(connfd), "r");
  xdrstdio_create(&xdrs_in, xfp_r, XDR_DECODE); 

  xfp_w = Fdopen(dup(connfd), "w");
  xdrstdio_create(&xdrs_out, xfp_w, XDR_ENCODE); 

  setbuf(xfp_w, NULL);
  setbuf(xfp_r, NULL);

  /* Main */
  fprintf(stdout, "Client starts.\n");

  /* XDR transfer op1 and op2 */
  fprintf(stdout, "Sending message.\n");

  if(!xdr_uint16_t(&xdrs_in, &op1)) {
    fprintf(stderr, "Error occurred while sending op1\n");
    return 1;
  }
  if(!xdr_uint16_t(&xdrs_in, &op2)) {
    fprintf(stderr, "Error occurred while sending op2\n");
    return 1;
  }
  fprintf(stdout, "Message received correctly.\n");

  /* Validating input */
  // TODO
  fprintf(stdout, "Both operands are correct.\n");
  fprintf(stdout, "op1 = %d\n", op1);
  fprintf(stdout, "op2 = %d\n", op2);

  /* XDR receive res */
  res = op1 + op2;
  xdr_uint16_t(&xdrs_out, &res);
  fprintf(stdout, "Server answered: %d.\n", res);
    
  xdr_destroy(&xdrs_in);
  xdr_destroy(&xdrs_out);
  fclose(xfp_r);
  fclose(xfp_w);
  fprintf(stdout, "Client ends.\n");
  return 0;
}


int main(int argc, char* argv[]){
  int			i, maxi, maxfd, listenfd, connfd, sockfd;
  int			nready, client[FD_SETSIZE];
  fd_set		rset, allset;
  socklen_t		clilen;
  struct sockaddr_in	cliaddr, servaddr;

  listenfd = Socket(AF_INET, SOCK_STREAM, 0);
  if(argc == 2){
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(atoi(argv[1]));
  }else{
    err_quit("usage: %s [ <host> ] <port#>", argv[0]);
  }

  Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

  fprintf(stdout, "Select Server starts.\n");

  Listen(listenfd, LISTENQ);

  maxfd = listenfd;			/* initialize */
  maxi = -1;				/* index into client[] array */
  for (i = 0; i < FD_SETSIZE; i++)
    client[i] = -1;			/* -1 indicates available entry */
  FD_ZERO(&allset);
  FD_SET(listenfd, &allset);
  /* end fig01 */

  /* include fig02 */
  for ( ; ; ) {
    rset = allset;		/* structure assignment */
    nready = Select(maxfd+1, &rset, NULL, NULL, NULL);

    if (FD_ISSET(listenfd, &rset)) {	/* new client connection */
      clilen = sizeof(cliaddr);
      connfd = Accept(listenfd, (SA *) &cliaddr, &clilen);
      printf("New connection accepted\n");
#ifdef	NOTDEF
      printf("new client: %s, port %d\n",
	     Inet_ntop(AF_INET, &cliaddr.sin_addr, 4, NULL),
	     ntohs(cliaddr.sin_port));
#endif

      for (i = 0; i < FD_SETSIZE; i++)
	if (client[i] < 0) {
	  client[i] = connfd;	        /* save descriptor */
	  break;
	}
      if (i == FD_SETSIZE)
	err_quit("too many clients");

      FD_SET(connfd, &allset);        	/* add new descriptor to set */
      if (connfd > maxfd)
	maxfd = connfd;			/* for select */
      if (i > maxi)
	maxi = i;			/* max index in client[] array */

      if (--nready <= 0)
	continue;			/* no more readable descriptors */
    }

    for (i = 0; i <= maxi; i++) {	/* check all clients for data */
      if ( (sockfd = client[i]) < 0)
	continue;
      if (FD_ISSET(sockfd, &rset)) {
	/* PAY */
	if (server_task(sockfd) == 1) {
	  Close(sockfd);
	  FD_CLR(sockfd, &allset);
	  client[i] = -1;
	}
	break;
      }
    }
  }
  fprintf(stdout, "Select Server stops.\n");
  return 0;
}
