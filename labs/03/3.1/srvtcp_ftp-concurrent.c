#include "../../../lib/lunp.h"

#define IBS 1024
#define OBS 1024
#define BS    80

void
sig_int(int signo){
  int i;

  fprintf(stdout, "In the SIG_INT handler\n");
  
  /* terminate all children */
  /* 
     If you are generating the SIGINT with Ctrl-c on a Unix system, then the signal is being sent to the entire process group.
  */
  /* DANGER ! USE IT WITH EXTREME CAUTION */

  while (wait(NULL) > 0)          /* wait for all children */
    ; 
  if (errno != ECHILD) 
    err_sys("wait error");

  exit(0);
}

void
sig_chld(int signo)
{
  pid_t   pid;
  int     stat;
  fprintf(stdout, "In the SIG_CHLD handler\n");
  /*  
  while ( (pid = waitpid(-1, &stat, WNOHANG)) > 0) {
    fprintf(stdout, "child %d terminated\n", pid);
  }
  */
  return;
}

void
sig_usr1(int signo){
  fprintf(stdout, "In the SIG_USR1 handler\n");
}

void
child_task(int connfd){
  /* Socket variables */
  char ibuf[IBS+1];
  char obuf[OBS+1];
  int size;
  size_t n;
  size_t tot=0;

  /* Internal variables */
  char tbuf[BS+1];
  char ifn[BS+1];
  char path[BS+1];
  FILE *ifp;
  int fsize;

  /* Flags*/
  int flag_quit = 0;

  /* Indexes */
  int i;
            
  /* Path */
  strcpy(path, "/Users/ldf/Desktop/srv/");
  
  fprintf(stdout, "Server starts.\n");

  /* Main loop MENU */
  while (!flag_quit) {
    fprintf(stdout, "* Ready to receive a command\n");
    n = Readline(connfd, ibuf, IBS);
    fprintf(stdout, "> Message: %7s\n", ibuf);

    // The readline returns n=1 
    if(ibuf[0] == '\0'  || strncmp(ibuf, "QUIT", 4)==0){ //Close the client's socket
      printf("Closing the connection!\n");
      flag_quit = 1;
    } else if (strncmp(ibuf, "GET", 3)==0) {
      fprintf(stdout, "* Requested a file\n");
      for (i=3; i<strlen(ibuf) 
	     && ibuf[i]!=' '
	     && ibuf[i]!='\n'; i++) {
	tbuf[i-3]=ibuf[i];
      }
      tbuf[i-3]='\0';
            
      strcpy(ifn, path);
      strcat(ifn, tbuf);
      fprintf(stdout, "nome file: %s\n", ifn);
      ifp = fopen(ifn, "rb");
            
      if (ifp==NULL) {
	fprintf(stderr, "  File not found!\n");
	strcpy(obuf, "-ERR\n");
	Writen(connfd, obuf, strlen(obuf));
      }else{
	strcpy(obuf, "+OK\n");
	Writen(connfd, obuf, strlen(obuf));

	/* Get size of the file */
	struct stat st;
        stat(ifn, &st);
        size = st.st_size;
	fsize = size;
	fprintf(stdout, "File's size: %d\n", size);

	size = htonl(size);

	memcpy(obuf, &size, sizeof(size));
	Writen(connfd, obuf, sizeof(size));

	while ((n=fread(obuf, 1, OBS, ifp))) {
	  Writen(connfd, obuf, (int)n); //Check the cast... 
	  tot += n;
	}
	fprintf(stdout, "Total bytes trasfered: %zd/%d\n", tot, fsize);
	fprintf(stdout, "Transfer is completed!\n");
      }
    }else{ //default
      fprintf(stdout, "Non recognised message!\n");
    }
  }
  fprintf(stdout, "Server quits.\n");
  return;
}

#define NCHILDREN_MAX 3
#define SIGC 3

int main(int argc, char *argv[]){
  int sigc = SIGC;
  void ( *sigv[SIGC] ) (int) = {sig_int, sig_chld, sig_usr1};
  int intv[SIGC] = {SIGINT, SIGCHLD, SIGUSR1};

  int nchildren_max = NCHILDREN_MAX;

  int			listenfd, connfd;
  pid_t			childpid;
  pid_t                 *pids_local;
  socklen_t		clilen, addrlen;
  struct sockaddr	*cliaddr;
  int                   nchildren_count;
  int                   i;
  sigset_t new_mask;
  sigset_t old_mask;
  int     stat;

  if (argc == 2)
    listenfd = Tcp_listen(NULL, argv[1], &addrlen);
  else if (argc == 3)
    listenfd = Tcp_listen(argv[1], argv[2], &addrlen);
  else{
    err_quit("usage: %s [ <host> ] <port#>", argv[0]);
  }

  cliaddr = Malloc(addrlen);

  /* Initialize data structure to memorize the pids */
  /* pids and nchildren are global variables defined in srv.h */
  pids_local = (int *)malloc(nchildren_max*sizeof(int));
  nchildren_count = 0;

  sigemptyset (&new_mask);
  sigaddset (&new_mask, SIGCHLD);

  for (i = 0; i < sigc; i++)
    Signal(intv[i], sigv[i]);

  for(i = 0; i <nchildren_max; i++)
    pids_local[i] = -1;

  for ( ; ; ) {
    clilen = addrlen;
    /* BLOCK THE FATHER if (nchildren_count >= nchildren_max) */
    Sigprocmask(SIG_BLOCK, &new_mask, &old_mask);
    for (i = 0; i < nchildren_max; i++){
      if(pids_local[i]>0 && Waitpid(pids_local[i], NULL, WNOHANG) > 0){
	pids_local[i] = -1;
	nchildren_count--;
      }
    }
    if(nchildren_count >= nchildren_max) {
      childpid = waitpid(-1, &stat, WNOHANG);
      for(i = 0; i < nchildren_max; i++){
	if(childpid == pids_local[i]) 
	  pids_local[i] = -1;
      }
      nchildren_count--;
    }
    Sigprocmask(SIG_BLOCK, &old_mask, NULL);
    if ( (connfd = accept(listenfd, cliaddr, &clilen)) < 0) {
      if (errno == EINTR)
	continue;                       /* back to for() */
      else
	err_sys("accept error");
    }

    if ( (childpid = Fork()) == 0) {	/* child process */
      Signal(SIGINT, NULL);             /* disable SIGINT handler in the child*/
      for(i=0; i<nchildren_max; i++){
	if(pids_local[i] == -1) 
	  pids_local[i] = childpid;
      }
      Close(listenfd);	                /* close listening socket */
      child_task(connfd);	        /* process request */
      exit(0);
    }

    Close(connfd);			/* parent closes connected socket */
    nchildren_count ++;
  }
  free(pids_local);
  return 0;
}
