#!/bin/bash

cd ..
find . -name '*~' -delete
find . -name '.DS_Store' -delete
cd examples
find . -type f -not -name '*.*' -delete
cd ../labs
find . -type f -not -name '*.*' -delete
